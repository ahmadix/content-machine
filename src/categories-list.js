import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { List } from 'native-base';

import CategoryEntry from './category-entry';

class CategoriesList extends Component {
    static propTypes = {
        categories: PropTypes.array.isRequired
    }

    render() {
        return (
            <List>
                {this.props.categories.map(category => (
                    <CategoryEntry key={category.id} category={category} />
                ))}
            </List>
        );
    }
}

export default CategoriesList;
