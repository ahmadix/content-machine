import React, { Component } from 'react';
import {
    View
} from 'react-native';

import StoriesList from './stories-list';

class HomeScreen extends Component {
    state = {
        stories: [
            { id: 1, title: 'first story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 2, title: 'second story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 3, title: 'third story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 4, title: 'fourth story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 5, title: 'fifth story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 6, title: 'sixth story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 7, title: 'seventh story title', img: 'https://placem.at/places?w=400&h=300' },
            { id: 8, title: 'eighth story title', img: 'https://placem.at/places?w=400&h=300' }
        ]
    }

    componentDidMount() {
        // send ajax request here and update the storeis list within state
    }

    render() {
        return (
            <View>
                <StoriesList stories={this.state.stories} />
            </View>
        );
    }
}

export default HomeScreen;
