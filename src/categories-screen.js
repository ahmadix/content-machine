import React, { Component } from 'react';
import {
    ScrollView
} from 'react-native';

import CategoriesList from './categories-list';

class CategoriesScreen extends Component {
    state = {
        categories: [
            { id: 1, title: 'first category title', img: 'https://placem.at/places?h=60' },
            { id: 2, title: 'second category title', img: 'https://placem.at/places?h=60' },
            { id: 3, title: 'third category title', img: 'https://placem.at/places?h=60' },
            { id: 4, title: 'fourth category title', img: 'https://placem.at/places?h=60' },
            { id: 5, title: 'fifth category title', img: 'https://placem.at/places?h=60' },
            { id: 6, title: 'sixth category title', img: 'https://placem.at/places?h=60' },
            { id: 7, title: 'seventh category title', img: 'https://placem.at/places?h=60' },
            { id: 8, title: 'eighth category title', img: 'https://placem.at/places?h=60' }
        ]
    }

    render() {
        return (
            <ScrollView>
                <CategoriesList categories={this.state.categories} />
            </ScrollView>
        );
    }
}

export default CategoriesScreen;
