import React, { Component } from 'react';
import {
    Container,
    Tabs,
    Tab,
    Header,
    Title
} from 'native-base';

import HomeScreen from './home-screen';
import CategoriesScreen from './categories-screen';

class ContentMachine extends Component {
    render() {
        return (
            <Container>
                <Header hasTabs>
                    <Title>Content Machine</Title>
                </Header>
                <Tabs>
                    <Tab heading="Stream">
                        <HomeScreen />
                    </Tab>

                    <Tab heading="Categories">
                        <CategoriesScreen />
                    </Tab>
                </Tabs>

            </Container>
        );
    }
}

export default ContentMachine;
