import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView
} from 'react-native';
import PropTypes from 'prop-types';

import StoryEntry from './story-entry';

const styles = StyleSheet.create({
    listView: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 30,
        backgroundColor: '#fcc'
    }
});

class StoriesList extends Component {
    static propTypes = {
        stories: PropTypes.array.isRequired
    }

    render() {
        return (
            <ScrollView style={styles.listView}>
                {this.props.stories.map(story => (<StoryEntry key={story.id} story={story} />))}
            </ScrollView>
        );
    }
}

export default StoriesList;
