import React, { Component } from 'react';
import {
    ListItem, Thumbnail, Text, Body, Toast

} from 'native-base';
import PropTypes from 'prop-types';

class CategoryEntry extends Component {
    static propTypes = {
        category: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);

        this.onCategoryPress = this.onCategoryPress.bind(this);
    }
    onCategoryPress() {
        const { title } = this.props.category;
        Toast.show({
            text: `${title} clicked!`,
            position: 'bottom',
            buttonText: 'Okay'
        });
    }

    render() {
        const { title, img } = this.props.category;
        return (
            <ListItem onPress={this.onCategoryPress}>
                <Thumbnail square size={80} source={{ uri: img }} />
                <Body>
                    <Text>{title}</Text>
                    <Text note>Its time to build a difference . .</Text>
                </Body>
            </ListItem>
        );
    }
}

export default CategoryEntry;
