import React, { Component } from 'react';
import {
    Image,
    Text,
    StyleSheet
} from 'react-native';
import {
    Card,
    CardItem
} from 'native-base';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
    card: {
        flex: 1,
        marginBottom: 15
    },
    cardItem: {
        flex: 1,
        maxHeight: 250,
        width: 400,
        height: 300
    }
});

class StoryEntry extends Component {
    static propTypes = {
        story: PropTypes.object.isRequired
    }

    render() {
        const { title, img } = this.props.story;
        return (
            <Card style={StyleSheet.flatten(styles.card)}>
                <CardItem cardBody>
                    <Image
                    style={styles.cardItem}
                    source={{ uri: img }} />
                </CardItem>
                <CardItem header>
                    <Text>{title}</Text>
                </CardItem>
            </Card>
        );
    }
}

export default StoryEntry;
