// @flow

import { AppRegistry } from 'react-native';
import ContentMachine from './src/content-machine';

AppRegistry.registerComponent('contentMachine', () => ContentMachine);
